#!/bin/bash

################################################################################

# BSD 3-Clause License
#
# Copyright (c) 2018, Quentin Lejard <zorval@valde.fr>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

################################################################################

################################################################################
##########                    Define color to output:                 ##########
################################################################################
_WHITE_="tput sgr0"
_RED_="tput setaf 1"
_GREEN_="tput setaf 2"
_ORANGE_="tput setaf 3"
################################################################################
# Path of git repository
# ./
GIT_PATH="$(realpath ${0%/*/*})"

RVPRX_CONTAINER="$1"
FQDN="$2"
PROJECT_IP_PRIV="$3"
CLOUD_TECH_ADMIN_EMAIL="$4"
CREATE_SSL_CERTIFICATES="$5"

echo "$($_ORANGE_)Generating certificates for: $FQDN $($_WHITE_)"
if $CREATE_SSL_CERTIFICATES ; then
    lxc exec $RVPRX_CONTAINER -- bash -c "certbot certonly -n --agree-tos --email $CLOUD_TECH_ADMIN_EMAIL --nginx -d $FQDN > /dev/null"
else
    echo "$($_GREEN_)CREATE_SSL_CERTIFICATES=false, don't create certificates, you need to setup it manually$($_WHITE_)"
fi

## Nginx vhost

echo "$($_ORANGE_)Nginx: Conf, Vhosts and tuning$($_WHITE_)"

# Test if /etc/nginx/RVPRX_common.conf file exist
lxc exec $RVPRX_CONTAINER -- bash -c "
    if [ ! -f /etc/nginx/RVPRX_common.conf ] ; then
        echo 'WARNING'
        echo 'No file /etc/nginx/RVPRX_common.conf are present'
        echo 'Needed for common nginx configuration'
        echo 'See https://framagit.org/zorval/lxd_deploy_apps/install_conf_lxd/blob/master/templates/rvprx/etc/nginx/RVPRX_common.conf'
    fi
"

echo "$($_ORANGE_)Create vhosts$($_WHITE_)"
#
echo "$($_ORANGE_)Create vhost: $FQDN $($_WHITE_)"
lxc file push $GIT_PATH/templates/rvprx/etc/nginx/sites-available/rvprx-wiki $RVPRX_CONTAINER/etc/nginx/sites-available/${FQDN}.conf
lxc exec $RVPRX_CONTAINER -- bash -c "
    sed -i                                          \
        -e 's/__FQDN_wiki__/$FQDN/'          \
        -e 's/__IP_PRIV_wiki__/$PROJECT_IP_PRIV/'        \
        /etc/nginx/sites-available/${FQDN}.conf
"

echo "$($_ORANGE_)Enable vhost$($_WHITE_)"
echo
lxc exec $RVPRX_CONTAINER -- bash -c "
    ln -sf /etc/nginx/sites-available/${FQDN}.conf      /etc/nginx/sites-enabled/
    if nginx -t ; then
        echo
        echo -e '\033[32mNginx conf OK, reload\033[0m'
        systemctl reload nginx
    else
        echo
        echo -e '\033[31m!!! ERROR !!! Nginx fail, NO realod and disable vhosts\033[0m'
        echo '${FQDN}.conf'
        rm -f /etc/nginx/sites-enabled/${FQDN}.conf

    fi
"
